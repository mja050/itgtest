//
//  GeneralTableViewCell.swift
//  ITGSampleProject
//
//  Created by Mo'min J.Abusaada on 17/04/2022.
//

import UIKit

protocol GeneralTableViewCellDelegate : NSObjectProtocol {
		/// using this protocols to help making the cell owned the actions
		func didselect(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
		func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
}

class GeneralTableViewCell: UITableViewCell,GeneralTableViewCellDelegate {
		
		weak open var tableView : GeneralTableView!
		open var indexPath : IndexPath!
		weak open var parentVC : UIViewController!
		weak open var object : GeneralTableViewModel!
		open var isSubObjectSelected : Bool = false
		weak open var delegate: GeneralTableViewCellDelegate?
		
		override func awakeFromNib() {
				super.awakeFromNib()
				// Initialization code
				self.delegate = self
		}
		
		override func setSelected(_ selected: Bool, animated: Bool) {
				super.setSelected(selected, animated: animated)
				
				// Configure the view for the selected state
		}
		
		func configerCell() {
				
		}
		func didselect(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
				
		}
		func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
				
		}
		func canSelect()->Bool{
				return true
		}
		deinit {
				debugPrint("GeneralTableViewCell  deinit")
		}
}
