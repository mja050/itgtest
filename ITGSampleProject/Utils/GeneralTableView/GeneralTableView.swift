//
//  GeneralTableView.swift
//  ITGSampleProject
//
//  Created by Mo'min J.Abusaada on 17/04/2022.
//

import UIKit
import DZNEmptyDataSet

enum GeneralTableViewType {
		case list
		case sections
}
enum GeneralTableViewSelectionType {
		case single
		case multi
}
/// this class is helper for table view to make add or remove any object easy
class GeneralTableView: UITableView {
		
		var dataSources = [UITableViewDataSource]()
		var delegates = [UITableViewDelegate]()
		
		var tableViewType : GeneralTableViewType = .list
		
		
		weak var rowHeightGlobal : NSNumber?
		weak var parentVC : UIViewController!
		var objects = [GeneralTableViewModel](){
				didSet{
						self.reloadData()
						self.emptyDataIsVisible = true
				}
		}
		var objectsOfSections = [(key: Any, value: Array<GeneralTableViewModel>)](){
				didSet{
						self.reloadData()
				}
		}
		
		var emptyDataSetTitle = "No Data To Show".localize_
		weak var emptyDataSetTitleColor:UIColor?
		var emptyDataIsVisible = false {
				didSet{
						self.reloadEmptyDataSet()
				}
		}
		
		var selectedObjects = [Any]()
		var primaryKeyOfSelection :String?
		var selectionType: GeneralTableViewSelectionType = .single
		var minimumSelectionCount = 0
		
		@IBInspectable var dummyActive: Bool = false
		@IBInspectable var dummyCellID: String = ""
		@IBInspectable var dummyObjectsCount: Int = 0
		
		
		var tableViewDidScroll: ((_ scrollView: UIScrollView) -> Void)?
		func didDidScroll(_ tableViewDidScroll: @escaping ((_ scrollView: UIScrollView) -> Void)) -> Void {
				self.tableViewDidScroll = tableViewDidScroll
		}
		private var willAddObject: ((_ object:Any) -> GeneralTableViewModel)?
		func handlerWillAddObject(_ willAddObject: @escaping  ((_ object:Any) -> GeneralTableViewModel)) -> GeneralTableView {
				self.willAddObject = willAddObject
				return self
		}
		private var reuseIdentifier: String = ""
		func reuseIdentifier(_ reuseIdentifier: String) -> GeneralTableView {
				self.reuseIdentifier = reuseIdentifier
				return self
		}
		
		private var heightForSections: ((_ section:Int) -> CGFloat)?
		func heightForSectionsFunc(_ heightForSections: @escaping ((_ section:Int) -> CGFloat)) -> GeneralTableView {
				self.heightForSections = heightForSections
				return self
		}
		
		private var viewForHeaderInSection: ((_ section:Int) -> UIView)?
		func viewForHeaderInSectionFunc(_ viewForHeaderInSection: @escaping ((_ section:Int) -> UIView)) -> GeneralTableView {
				self.viewForHeaderInSection = viewForHeaderInSection
				return self
		}
		
		
		private var titleForEmptyDataSet: NSAttributedString?
		func handlerTitleForEmptyDataSet(_ titleForEmptyDataSet: NSAttributedString?) -> GeneralTableView {
				self.titleForEmptyDataSet = titleForEmptyDataSet
				return self
		}
		private var descriptionForEmptyDataSet: NSAttributedString?
		func handlerDescriptionForEmptyDataSet(_ descriptionForEmptyDataSet: NSAttributedString?) -> GeneralTableView {
				self.descriptionForEmptyDataSet = descriptionForEmptyDataSet
				return self
		}
		private var imageForEmptyDataSet: UIImage?
		func handlerImageForEmptyDataSet(_ imageForEmptyDataSet: UIImage?) -> GeneralTableView {
				self.imageForEmptyDataSet = imageForEmptyDataSet
				return self
		}
		
		private var canEditRowAtIndexPath : ((_ IndexPath:IndexPath) -> Bool)?
		func handlerCanEditRowAt(_ indexPath:@escaping ((_ IndexPath:IndexPath) -> Bool)) -> GeneralTableView {
				self.canEditRowAtIndexPath = indexPath
				return self
		}
		private var commitEditingStyleForRowAt : ((_ editingStyle: UITableViewCell.EditingStyle,_ IndexPath:IndexPath,_ object:GeneralTableViewModel) -> Void)?
		func handlerCommitEditingStyleForRowAt(_ indexPath:@escaping ((_ editingStyle: UITableViewCell.EditingStyle,_ IndexPath:IndexPath,_ object:GeneralTableViewModel) -> Void)) -> GeneralTableView {
				self.commitEditingStyleForRowAt = indexPath
				return self
		}
		
		/*
		 // Only override draw() if you perform custom drawing.
		 // An empty implementation adversely affects performance during animation.
		 override func draw(_ rect: CGRect) {
		 // Drawing code
		 }
		 */
		override func awakeFromNib() {
				super.awakeFromNib()
				self.parentVC = self.getParentViewController()
				self.delegate = self
				self.dataSource = self
				self.emptyDataSetDelegate = self
				self.emptyDataSetSource = self
		}
		
		override func didMoveToWindow() {
				super.didMoveToWindow()
				/// if you select to present dummy data from storyboard inspector this code will add a dummy cells
				if dummyActive == true && dummyCellID.count > 0 && dummyObjectsCount > 0 {
						self.clearData(true)
						for _ in 0...dummyObjectsCount {
								self.objects.append(GeneralTableViewModel(reuseIdentifier: dummyCellID, object: nil, rowHeight: nil))
						}
						self.reloadData()
				}
		}
		/// helper to register the xib file for cells
		func registerNib(NibName: String) {
				self.register(UINib(nibName: NibName, bundle: nil), forCellReuseIdentifier: NibName)
		}
		/// using this function to get the content size
		func updateHeightBaseOnContent(constraint:NSLayoutConstraint) {
				constraint.constant = CGFloat.greatestFiniteMagnitude
				self.reloadData()
				self.layoutIfNeeded()
				constraint.constant = self.contentSize.height
		}
		deinit {
				debugPrint("GeneralTableView  deinit")
		}
}

//MARK: - DataHelper
extension GeneralTableView {
		/// clear all table view data with option to reload or not
		func clearData(_ reloadData: Bool = false){
				if self.tableViewType == .sections {
						self.objectsOfSections.removeAll()
				}else{
						self.objects.removeAll()
				}
				if reloadData == true {
						self.reloadData()
				}
		}
		/// you can use the selectedObjects array to check if user click on did select for this item or not
		func addRemoveSelectedObject(_ object: Any) {
				let check = GeneralTableViewModel.checkIfObjectExist(primaryKeyOfSelection: self.primaryKeyOfSelection, object, arr: self.selectedObjects)
				if check.0 == true{
						if self.minimumSelectionCount == 0 || self.minimumSelectionCount != self.selectedObjects.count {
								let index = check.2
								self.selectedObjects.remove(at: index)
						}
				} else {
						if selectionType == .single {
								self.selectedObjects = [object]
						} else {
								self.selectedObjects.append(object)
						}
				}
				self.reloadData()
		}
		/// adding array of objects to tableview by passing the reuseIdentifier and if you need to clear the table before adding the items
		func appendData(reuseIdentifier: String, objects:[Any],clear:Bool = false){
				if clear == true {
						self.clearData(true)
				}
				objects.forEach {[weak self] obj in
						self?.appendData(reuseIdentifier: reuseIdentifier, object: obj,reloadData: false)
				}
				self.reloadData()
		}
		/// adding object to tableview by passing the reuseIdentifier and if you need to reload the table after adding the items
		func appendData(reuseIdentifier: String, object:Any,reloadData:Bool = true){
				if GeneralTableViewModel.checkIfObjectExist(primaryKeyOfSelection: primaryKeyOfSelection, object,arr:self.objects).0 == false {
						self.objects.append(self.willAddObject?(object) ?? GeneralTableViewModel(reuseIdentifier: reuseIdentifier, object: object, rowHeight: nil))
				}
				if reloadData == true {
						self.reloadData()
				}
		}
}

//MARK: - TableViewDelegate
extension GeneralTableView : UITableViewDelegate {
		func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
				return self.tableViewType == .sections ? self.heightForSections?(section) ?? 44 : 0
		}
		func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
				return self.tableViewType == .sections ? self.viewForHeaderInSection?(section) ?? UIView() : nil
		}
		
		public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
				let value = self.tableViewType == .sections ? self.objectsOfSections[indexPath.section].value[indexPath.row].rowHeight : objects[indexPath.row].rowHeight
				return (value != nil) ? CGFloat((value?.floatValue)!) : UITableView.automaticDimension
		}
		public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
				let value = self.tableViewType == .sections ? self.objectsOfSections[indexPath.section].value[indexPath.row].rowHeight : objects[indexPath.row].rowHeight
				return (value != nil) ? CGFloat((value?.floatValue)!) : UITableView.automaticDimension
		}
		
		func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
				let obj = self.tableViewType == .sections ? self.objectsOfSections[indexPath.section].value[indexPath.row] : objects[indexPath.row]
				let cell = tableView.cellForRow(at: indexPath) as! GeneralTableViewCell
				/// check if cell can select to add or remove the object from selected array
				if let obb = obj.object , cell.canSelect() == true {
						self.addRemoveSelectedObject(obb)
				}
				/// if you need to use multi delegate for table view
				for del in self.delegates {
						del.tableView?(tableView, didSelectRowAt: indexPath)
				}
				/// using cell delegate to passing the value of didselect
				cell.delegate?.didselect(tableView, didSelectRowAt: indexPath)
		}
		func scrollViewDidScroll(_ scrollView: UIScrollView) {
				self.tableViewDidScroll?(scrollView)
		}
}

//MARK: - TableViewDataSource
extension GeneralTableView : UITableViewDataSource {
		func numberOfSections(in tableView: UITableView) -> Int {
				return self.tableViewType == .sections ? self.objectsOfSections.count : 1
		}
		func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
				return self.tableViewType == .sections ? self.objectsOfSections[section].value.count : objects.count
		}
		func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
				let obj = self.tableViewType == .sections ? self.objectsOfSections[indexPath.section].value[indexPath.row] : objects[indexPath.row]
				let cell = tableView.dequeueReusableCell(withIdentifier: obj.reuseIdentifier, for: indexPath) as! GeneralTableViewCell
				cell.tableView = self
				cell.indexPath = indexPath
				cell.parentVC = self.parentVC
				cell.object = obj
				/// check if the object is selected and in the selected objects array
				if let obb = obj.object {
						cell.isSubObjectSelected = GeneralTableViewModel.checkIfObjectExist(primaryKeyOfSelection: self.primaryKeyOfSelection, obb, arr: self.selectedObjects).0
				}
				cell.configerCell()
				return cell
		}
		func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
				return self.canEditRowAtIndexPath?(indexPath) ?? false
		}
		func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
				let obj = self.tableViewType == .sections ? self.objectsOfSections[indexPath.section].value[indexPath.row] : objects[indexPath.row]
				self.commitEditingStyleForRowAt?(editingStyle,indexPath,obj)
				
				let cell = tableView.cellForRow(at: indexPath) as! GeneralTableViewCell
				cell.delegate?.tableView(tableView, commit: editingStyle, forRowAt: indexPath)
				
				for dataS in self.dataSources{
						dataS.tableView?(tableView, commit: editingStyle, forRowAt: indexPath)
				}
		}
		
}

//MARK: - EmptyDataSet
extension GeneralTableView : DZNEmptyDataSetSource,DZNEmptyDataSetDelegate {
		func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString {
				let attributes: [AnyHashable: Any] = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18), NSAttributedString.Key.foregroundColor: emptyDataSetTitleColor ?? UIColor.darkGray]
				return self.titleForEmptyDataSet ?? NSAttributedString(string: emptyDataSetTitle, attributes: attributes as? [NSAttributedString.Key : Any])
		}
		func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
				return self.descriptionForEmptyDataSet ?? NSAttributedString(string: "")
		}
		func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
				return self.imageForEmptyDataSet ?? UIImage()
		}
		func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
				return true
		}
		func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
				return self.emptyDataIsVisible
		}
}
