//
//  GeneralTableViewModel.swift
//  ITGSampleProject
//
//  Created by Mo'min J.Abusaada on 17/04/2022.
//

import Foundation

class GeneralTableViewModel: NSObject {
		
		weak var rowHeight: NSNumber?
		var reuseIdentifier: String = ""
		var object: Any?
		/// this object for fill the table array
		/// - reuseIdentifier : that cell need to add
		/// - object : the inner object that need to passing to cell
		/// - rowHeight : if you need a specific row height
		init(reuseIdentifier: String, object: Any?, rowHeight:NSNumber? = nil) {
				self.reuseIdentifier = reuseIdentifier
				self.object = object
				self.rowHeight = rowHeight
				super.init()
		}
		deinit {
				debugPrint("GeneralListViewData  deinit")
		}
		/// this function to check array if thiere is any duplecated objects
		/// - primaryKeyOfSelection : this key for check is equal
		/// - object : this object you need to check if exist
		/// - arr : the array of objects you need to check
		/// - removeIfFound : bool if you need to remove the object if founded
		static func checkIfObjectExist(primaryKeyOfSelection:String?,_ object:Any,arr:[Any]?,removeIfFound:Bool = false) -> (Bool,Any?,Int) {
				let key = primaryKeyOfSelection ?? "pk_i_id"
				var found : Any?
				var index = 0
				for obj in arr ?? [] {
						let hasClassMember = (obj as AnyObject).responds(to: Selector((key)))
						let hasObjectClassMember = (object as AnyObject).responds(to: Selector((key)))
						if hasClassMember == true && hasObjectClassMember == true{
								let valueOfObj = (obj as AnyObject).value(forKey: key)
								let valueOfobject = (object as AnyObject).value(forKey: key)
								if (valueOfObj != nil && valueOfobject != nil) {
										if valueOfObj is NSNumber && valueOfobject is NSNumber && (valueOfObj as! NSNumber).isEqual(to: valueOfobject as! NSNumber){
												found = obj
												break
										}else if (valueOfObj as AnyObject).description == (valueOfobject as AnyObject).description {
												found = obj
												break
										}
								}else if (obj as AnyObject).value(forKey: key) == nil || (obj as AnyObject).value(forKey: key) == nil {
										found = nil
								}
						}else if (obj as AnyObject).description == (object as AnyObject).description{
								found = obj
								break
						}
						index = index + 1
				}
				return (found != nil,found,index)
		}

}
