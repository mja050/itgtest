//
//  UIViewControllerEx.swift
//  ITGSampleProject
//
//  Created by Mo'min J.Abusaada on 17/04/2022.
//

import UIKit

extension NSObject {
		@nonobjc class var className_: String{
				return NSStringFromClass(self).components(separatedBy: ".").last!
		}
}
extension UIViewController{
		
		func showPopAlert(title:String,message msg:String,okAction:(() -> Void)? = nil)  {
				let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
				let defaultAction = UIAlertAction(title:"OK".localize_, style: .default, handler: { (pAlert) in
						//Do whatever you wants here
						okAction?()
				})
				alertController.addAction(defaultAction)
				self.present(alertController, animated: true, completion: nil)
		}
		func showErrorMessage(message msg:String,okAction:(() -> Void)? = nil) {
				self.showPopAlert(title: "Error".localize_, message: msg,okAction: okAction)
		}
		
		var topbarHeight: CGFloat {
				return UIApplication.shared.statusBarFrame.size.height +
				(self.navigationController?.navigationBar.frame.height ?? 0.0)
		}
		func performSegueWithCheck(withIdentifier identifier: String, sender: Any?) {
				if canPerformSegue(withIdentifier: identifier){
						self.performSegue(withIdentifier: identifier, sender: sender)
				}
		}
		func canPerformSegue(withIdentifier identifier: String) -> Bool {
				//first fetch segue templates set in storyboard.
				guard let identifiers = value(forKey: "storyboardSegueTemplates") as? [NSObject] else {
						//if cannot fetch, return false
						return false
				}
				//check every object in segue templates, if it has a value for key _identifier equals your identifier.
				let canPerform = identifiers.contains { (object) -> Bool in
						if let id = object.value(forKey: "_identifier") as? String {
								if id == identifier{
										return true
								} else {
										return false
								}
						} else {
								return false
						}
				}
				return canPerform
		}
		
		@IBInspectable
		var localized: String? {
				get {
						return self.title
				}
				set {
						self.title = newValue?.localize_
				}
		}
		@IBAction func btnPopViewController(_ sender: Any) {
				self.navigationController?.popViewController(animated: true)
		}
		
		@IBAction func btnDismissViewController(_ sender: Any) {
				self.dismiss(animated: true, completion: nil)
		}
}
