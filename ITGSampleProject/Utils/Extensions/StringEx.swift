//
//  StringEx.swift
//  ITGSampleProject
//
//  Created by Mo'min J.Abusaada on 17/04/2022.
//

import UIKit

extension String {
		func index(from: Int) -> Index {
				return self.index(startIndex, offsetBy: from)
		}
		
		func substring(from: Int) -> String {
				let fromIndex = index(from: from)
				return substring(from: fromIndex)
		}
		
		func substring(to: Int) -> String {
				let toIndex = index(from: to)
				return substring(to: toIndex)
		}
		
		func substring(with r: Range<Int>) -> String {
				let startIndex = index(from: r.lowerBound)
				let endIndex = index(from: r.upperBound)
				return substring(with: startIndex..<endIndex)
		}
		
		func heightForWithFont(font: UIFont, width: CGFloat, insets: UIEdgeInsets) -> CGFloat {
				
				let label:UILabel = UILabel(frame: CGRect(x:0, y:0,width: width + insets.left + insets.right,height: CGFloat.greatestFiniteMagnitude))
				label.numberOfLines = 0
				label.lineBreakMode = NSLineBreakMode.byWordWrapping
				label.font = font
				label.text = self
				
				label.sizeToFit()
				return label.frame.height + insets.top + insets.bottom
		}
		
		func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
				let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
				let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
				
				return boundingBox.height
		}
		
		func isValidEmail() -> Bool {
				// print("validate calendar: \(testStr)")
				let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
				
				let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
				return emailTest.evaluate(with: self)
		}
		
		mutating func stripHTML() -> String {
				self = self.replacingOccurrences(of: "&ndash;", with: "-")
				self = self.replacingOccurrences(of: "&nbsp;", with: " ")
				return self.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
		}
		func matchingStrings(regex: String) -> [[String]] {
				guard let regex = try? NSRegularExpression(pattern: regex, options: []) else { return [] }
				let nsString = self as NSString
				let results  = regex.matches(in: self, options: [], range: NSMakeRange(0, nsString.length))
				return results.map { result in
						(0..<result.numberOfRanges).map { result.range(at: $0).location != NSNotFound
								? nsString.substring(with: result.range(at: $0))
								: ""
						}
				}
		}
		
		func URLEncodedString() -> String? {
				return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
		}
		func isEmptyOrWhiteSpace()->Bool{
				let str = self
				return (str ?? "").replacingOccurrences(of: " ", with: "").count == 0
		}
		var localize_ : String {
				return NSLocalizedString(self, comment: "")
		}
		func localized(with variables: CVarArg...) -> String {
				return String(format: self.localize_, arguments: variables)
		}
		func toDictionary() -> Dictionary<String, Any> {
				if let data = self.data(using: String.Encoding.utf8) {
						do {
								let jsonDictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
								return jsonDictionary ?? [:]
						} catch {
								return [:]
						}
				}
				return [:]
		}
		var removeWhiteSpaceAndArabicNumbers : String{
				return self.replacingOccurrences(of: " ", with: "").removeArabicNumbers
		}
		var removeArabicNumbers : String{
				var str = self
				let map = ["٠": "0",
									 "١": "1",
									 "٢": "2",
									 "٣": "3",
									 "٤": "4",
									 "٥": "5",
									 "٦": "6",
									 "٧": "7",
									 "٨": "8",
									 "٩": "9"]
				map.forEach { str = str.replacingOccurrences(of: $0, with: $1) }
				return str
		}
		var image_ : UIImage? {
				return UIImage(named: self)
		}
		func applyPatternOnNumbers(pattern: String, replacmentCharacter: Character) -> String {
				var pureNumber = self.replacingOccurrences( of: "[^0-9]", with: "", options: .regularExpression)
				for index in 0 ..< pattern.count {
						guard index < pureNumber.count else { return pureNumber }
						let stringIndex = String.Index(encodedOffset: index)
						let patternCharacter = pattern[stringIndex]
						guard patternCharacter != replacmentCharacter else { continue }
						pureNumber.insert(patternCharacter, at: stringIndex)
				}
				return pureNumber
		}
		var color_: UIColor {
				return UIColor(named: self) ?? UIColor.hexColor(hex: self)
		}
		func queryURL(params: [String: String]) -> String {
				var components = URLComponents(string: self)
				components?.queryItems = params.map { element in URLQueryItem(name: element.key, value: element.value) }
				return components?.url?.absoluteString ?? self
		}
}

