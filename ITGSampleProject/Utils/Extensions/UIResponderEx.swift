//
//  UIResponderEx.swift
//  ITGSampleProject
//
//  Created by Mo'min J.Abusaada on 17/04/2022.
//

import UIKit

extension UIResponder {
		func getParentViewController() -> UIViewController? {
				if self.next is UIViewController {
						return self.next as? UIViewController
				} else {
						if self.next != nil {
								return (self.next!).getParentViewController()
						}
						else {return nil}
				}
		}
}
