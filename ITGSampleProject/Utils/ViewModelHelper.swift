//
//  ViewModelHelper.swift
//  ITGSampleProject
//
//  Created by Mo'min J.Abusaada on 17/04/2022.
//

import Foundation

/// Box generic class is to make data transfare easy to use by changing only the value and notifiy by using bind function
final class Box<T> {

		typealias Listener = (T) -> Void
		var listener: Listener?
		
		var value: T {
				didSet {
						listener?(value)
				}
		}
		
		init(_ value: T) {
				self.value = value
		}
		
		func bind(listener: Listener?) {
				self.listener = listener
				listener?(value)
		}
}
