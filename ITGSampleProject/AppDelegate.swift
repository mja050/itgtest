//
//  AppDelegate.swift
//  ITGSampleProject
//
//  Created by Mo'min J.Abusaada on 17/04/2022.
//

import UIKit
import CoreData
import MagicalRecord
import CocoaDebug

@main
class AppDelegate: UIResponder, UIApplicationDelegate {


	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
			self.setupCoreDataHelper()
			/// CocoaDebug is fore tracking all network requests sent from application
			CocoaDebug.enable()
			
		return true
	}

	// MARK: UISceneSession Lifecycle

	func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
		// Called when a new scene session is being created.
		// Use this method to select a configuration to create the new scene with.
		return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
	}

	func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
		// Called when the user discards a scene session.
		// If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
		// Use this method to release any resources that were specific to the discarded scenes, as they will not return.
	}
	// MARK: - Core Data Saving support
	func saveContext () {
			/// save NSManagedObjectContext for MagicalRecord
			NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
	}

}


extension AppDelegate {
		func setupCoreDataHelper(){
				/// setup MagicalRecord to set the coredata name
				MagicalRecord.setupCoreDataStack(withStoreNamed: "ITGSampleProject")
		}
}
