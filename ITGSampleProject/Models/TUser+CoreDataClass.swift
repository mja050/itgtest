//
//  TUser+CoreDataClass.swift
//  
//
//  Created by Mo'min J.Abusaada on 18/04/2022.
//
//

import Foundation
import CoreData
import ObjectMapper
import MagicalRecord


@objc(TUser)
public class TUser:NSManagedObject,Mappable{
		
		@objc
		private override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
				super.init(entity: entity, insertInto: context)
		}

		// MARK: Mapping
		required public init?(map: Map) {
				let ctx = NSManagedObjectContext.mr_default()
				if let login = map.JSON["login"] as? String,let user = TUser.mr_findFirst(with: NSPredicate(format: "login = %@", login)) {
						super.init(entity: user.entity, insertInto: ctx)
						mapping(map: map)
						return
				}
				let entity = NSEntityDescription.entity(forEntityName: "TUser", in: ctx)
				super.init(entity: entity!, insertInto: ctx)
				
				mapping(map: map)
		}
		// Mappable
		public func mapping(map: Map) {
				login   	 		<- map["login"]
				id         		<- map["id"]
				avatar_url		<- map["avatar_url"]
				url						<- map["url"]
				html_url			<- map["html_url"]
				followers			<- map["followers"]
				following			<- map["following"]
				public_repos	<- map["public_repos"]
				public_gists	<- map["public_gists"]
		}
}
