//
//  TUser+CoreDataProperties.swift
//  
//
//  Created by Mo'min J.Abusaada on 18/04/2022.
//
//

import Foundation
import CoreData

extension TUser {
		
		@nonobjc public class func fetchRequest() -> NSFetchRequest<TUser> {
				return NSFetchRequest<TUser>(entityName: "TUser")
		}
		
		@NSManaged public var login: String?
		@NSManaged public var id: NSNumber
		@NSManaged public var avatar_url: String?
		@NSManaged public var url: String?
		@NSManaged public var html_url: String?
		@NSManaged public var followers: NSNumber
		@NSManaged public var following: NSNumber
		@NSManaged public var public_repos: NSNumber
		@NSManaged public var public_gists: NSNumber
		
}
