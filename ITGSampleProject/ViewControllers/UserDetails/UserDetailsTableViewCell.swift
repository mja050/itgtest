/************************* Mo’min J.Abusaada *************************/
//
//  UserDetailsTableViewCell.swift
//  ITGSampleProject
//
//  Created by Mo'min J.Abusaada on 18/04/2022.
//

import UIKit

enum UserDetailsTableViewCellType:CaseIterable {
		case url
		case html_url
		case followers
		case following
		case public_repos
		case public_gists
		
		var s_title:String {
				switch self {
						case .url:
								return "UserDetailsTableViewCellType_url".localize_
						case .html_url:
								return "UserDetailsTableViewCellType_html_url".localize_
						case .followers:
								return "UserDetailsTableViewCellType_followers".localize_
						case .following:
								return "UserDetailsTableViewCellType_following".localize_
						case .public_repos:
								return "UserDetailsTableViewCellType_public_repos".localize_
						case .public_gists:
								return "UserDetailsTableViewCellType_public_gists".localize_
				}
		}
		func getDetails(_ user:TUser)->String?{
				switch self {
						case .url:
								return user.url
						case .html_url:
								return user.html_url
						case .followers:
								return user.followers.stringValue
						case .following:
								return user.following.stringValue
						case .public_repos:
								return user.public_repos.stringValue
						case .public_gists:
								return user.public_gists.stringValue
				}
		}
}

class UserDetailsTableViewCell : GeneralTableViewCell {

		@IBOutlet weak var lblTitle: UILabel!
		@IBOutlet weak var lblDetails: UILabel!
		
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
		///This function called every time tableview call tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)
    override func configerCell() {
				if let user = (self.parentVC as? UserDetailsViewController)?.object,
					 let obj = self.object.object as? UserDetailsTableViewCellType{
						self.lblTitle.text = obj.s_title
						self.lblDetails.text = obj.getDetails(user)
				}
    }
		///This function call every time the didselect(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) from table
    override func didselect(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
				if let user = (self.parentVC as? UserDetailsViewController)?.object,
					 let obj = self.object.object as? UserDetailsTableViewCellType{
						/// open the web url when the user click on any cell of types (url or html_url) and when the url is not correct of empty will present an error alert
						if (obj == .url || obj == .html_url),let url = URL(string:obj.getDetails(user) ?? "") {
								if UIApplication.shared.canOpenURL(url) == true {
										UIApplication.shared.open(url, options: [:], completionHandler: nil)
								}else {
										self.parentVC.showErrorMessage(message: "Sorry, there is no url to open it".localize_, okAction: nil)
								}
						}
				}
    }
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
}
