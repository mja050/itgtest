//
//  UserDetailsViewController.swift
//  ITGSampleProject
//
//  Created by Mo'min J.Abusaada on 18/04/2022.
//

import UIKit

class UserDetailsViewController: UIViewController {

		@IBOutlet weak var tableView: GeneralTableView!
		private let refreshControl = UIRefreshControl()

		var object:TUser?
		private var viewModel = UserDetailsViewModel()
		
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
				self.setupView()
				self.setupViewModel()
				self.setupData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UserDetailsViewController {
		/// This function for setup the tableview and title of the viewcontroller with setup the refreshControl of tableview
		func setupView(){
				self.title = self.object?.login
				self.tableView.registerNib(NibName: UsersListTableViewCell.className_)
				self.tableView.registerNib(NibName: UserDetailsTableViewCell.className_)
				
				self.tableView.refreshControl = self.refreshControl
				self.refreshControl.addTarget(self, action: #selector(setupViewModel), for: .valueChanged)
		}
		/// This function for calling the viewModel to get data from API with the name of user
		@objc func setupViewModel(){
				self.viewModel.getData(username:self.object?.login ?? "") { [weak self] in
						/// Stop refreshControl animation
						self?.tableView.refreshControl?.endRefreshing()
				}
		}
		/// This function for calling callback when the data return from API
		func setupData(){
				self.viewModel.data.bind {[weak self] user in
						guard let self = self else { return }
						self.object = user
						
						/// clear the tableview with reload and user object returned from API adding to tableview objects array as first item and make a foreloop for enum UserDetailsTableViewCellType that contans all types of userdetails view controller elements 
						self.tableView.clearData(true)
						self.tableView.appendData(reuseIdentifier: UsersListTableViewCell.className_, object: user)
						self.tableView.appendData(reuseIdentifier: UserDetailsTableViewCell.className_, objects: UserDetailsTableViewCellType.allCases)
				}
		}
}
