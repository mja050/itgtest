/************************* Mo’min J.Abusaada *************************/
//
//  UsersListTableViewCell.swift
//  ITGSampleProject
//
//  Created by Mo'min J.Abusaada on 18/04/2022.
//

import UIKit
import SDWebImage

class UsersListTableViewCell : GeneralTableViewCell {
		
		@IBOutlet weak var imgUserAvatare: UIImageView!
		@IBOutlet weak var lblUserID: UILabel!
		@IBOutlet weak var lblUserName: UILabel!
		override func awakeFromNib() {
				super.awakeFromNib()
				// Initialization code
		}
		
		override func setSelected(_ selected: Bool, animated: Bool) {
				super.setSelected(selected, animated: animated)
				
				// Configure the view for the selected state
		}
		///This function called every time tableview call tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)
		override func configerCell() {
				if let obj = self.object.object as? TUser{
						self.imgUserAvatare.sd_setImage(with: URL(string: obj.avatar_url ?? ""), placeholderImage: "Missing_avatar".image_, options: .refreshCached)
						self.lblUserID.text = "UserID".localized(with: obj.id.stringValue)
						self.lblUserName.text = "UserName".localized(with: obj.login ?? "")
				}
		}
		///This function call every time the didselect(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) from table
		override func didselect(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
				/// remove selection background in user details view controller
				if self.parentVC is UserDetailsViewController {
						tableView.deselectRow(at: indexPath, animated: true)
						return
				}
				/// Push the user details view controller after make sure the user and the view controller is not nil
				if let obj = self.object.object as? TUser,let vc = self.parentVC.storyboard?.instantiateViewController(withIdentifier: UserDetailsViewController.className_) as? UserDetailsViewController{
						vc.object = obj
						self.parentVC.navigationController?.pushViewController(vc, animated: true)
				}
		}
		override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
				
		}
}
