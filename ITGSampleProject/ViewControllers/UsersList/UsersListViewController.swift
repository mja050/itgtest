//
//  UsersListViewController.swift
//  ITGSampleProject
//
//  Created by Mo'min J.Abusaada on 18/04/2022.
//

import UIKit

class UsersListViewController: UIViewController {

		@IBOutlet weak var tableView: GeneralTableView!
		private let refreshControl = UIRefreshControl()

		private let viewModel = UsersListViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
				self.setupView()
				self.setupViewModel()
				self.setupData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UsersListViewController {
		/// This function for setup the tableview and title of the viewcontroller with setup the refreshControl of tableview
		func setupView(){
				self.title = "UsersListVC_title".localize_
				self.tableView.registerNib(NibName: UsersListTableViewCell.className_)
				
				self.tableView.refreshControl = self.refreshControl
				self.refreshControl.addTarget(self, action: #selector(setupViewModel), for: .valueChanged)
		}
		/// This function for calling the viewModel to get data from API
		@objc func setupViewModel(){
				self.viewModel.getData() { [weak self] in
						/// Stop refreshControl animation
						self?.tableView.refreshControl?.endRefreshing()
				}
		}
		/// This function for calling callback when the data return from API
		func setupData(){
				self.viewModel.data.bind {[weak self] users in
						guard let self = self else { return }
						
						/// clear the tableview with reload and forEach users array returned from API and adding elements to tableview objects array
						self.tableView.appendData(reuseIdentifier: UsersListTableViewCell.className_, objects: users,clear: true)
				}
		}
}
