//
//  ResponseHandler.swift
//  ITGSampleProject
//
//  Created by Mo'min J.Abusaada on 18/04/2022.
//

import Moya
import Moya_ObjectMapper
import ObjectMapper
import MagicalRecord

/// this class to check each request if there is any error or server down or any problem with Api endpoint
public class ResponseHandler<T:TargetType,M: Mappable>{
		static func checkResponse(target: T,completionResponse:@escaping (([M])->Void),finishRequest: (()->())? = nil){
				MoyaProvider<T>().request(target, completion: { result in
						/// show error message in root view controller
						func showErrorMassage(_ msg:String){
								let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
								keyWindow?.rootViewController?.showErrorMessage(message: msg)
						}
						var message = "Unable to fetch from GitHub".localize_
						finishRequest?()
						switch result {
								case let .success(response):
										do {
												/// mapping the response and adding it to coredata by using MagicalRecord framework
												if let arr = try? response.mapArray(M.self) {
														completionResponse(arr)
												}else if let obj = try? response.mapObject(M.self) {
														completionResponse([obj])
												} else {
														showErrorMassage(message)
												}
												/// saving the data that added to coredata or updated
												(UIApplication.shared.delegate as? AppDelegate)?.saveContext()
										} catch {
												showErrorMassage(message)
										}
								case let .failure(error):
										guard let error = error as? CustomStringConvertible else {
												showErrorMassage(message)
												break
										}
										message = error.description
										showErrorMassage(message)
						}
				})
		}
}
