//
//  GitHubRequets.swift
//  ITGSampleProject
//
//  Created by Mo'min J.Abusaada on 17/04/2022.
//

import Foundation
import Moya
/// Api requests endpoints
enum GitHubService {
		case allUsers
		case showUser(username: String)
}

// MARK: - TargetType Protocol Implementation

extension GitHubService: TargetType {
		
		var baseURL: URL { return URL(string: APIConstents.api_url)! }
		
		var path: String {
				switch self {
						case .allUsers:
								return APIConstents.Api_users_url
						case .showUser(let username):
								return APIConstents.Api_show_user_url + username
				}
		}
		
		var method: Moya.Method {
				switch self {
						case .allUsers, .showUser:
								return .get
				}
		}
		
		var parameters: [String: Any]? {
				switch self {
						case .allUsers,.showUser:
								return nil
				}
		}
		
		var parameterEncoding: ParameterEncoding {
				switch self {
						case .allUsers, .showUser:
								return URLEncoding.default
				}
		}
		
		var sampleData: Data {
				switch self {
						case .allUsers, .showUser:
								return "".utf8Encoded
				}
		}
		var headers: [String : String]? {
				return nil
		}
		var task: Task {
				switch self {
						case .allUsers, .showUser:
								return .requestPlain
				}
		}
}

// MARK: - Helpers
private extension String {
		var urlEscaped: String {
				return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
		}
		
		var utf8Encoded: Data {
				return self.data(using: .utf8)!
		}
}

