//
//  UserDetailsViewModel.swift
//  ITGSampleProject
//
//  Created by Mo'min J.Abusaada on 18/04/2022.
//

import Moya
import Moya_ObjectMapper
import MagicalRecord

class UserDetailsViewModel {
		
		var data:Box<TUser?> = Box(nil)

		/// getData will send the request to API and get the the data as object and check the response inside the ResponseHandler class
		/// - finishRequest : this call back will return when the request return the response regardless of the result of the api is success or fail
		func getData(username:String,finishRequest: (()->())? = nil){
				ResponseHandler<GitHubService, TUser>.checkResponse(target: .showUser(username: username)) { [weak self] arr in
						/// check if there is no data returned from api will get the data from coredata by username
						var user = arr.first
						if user == nil {
								user = TUser.mr_findFirst(with: NSPredicate(format: "login = %@", username))
						}
						/// the data will return as object and will set the data to Box generic class that notify when value changes
						self?.data.value = user
				} finishRequest: {
						finishRequest?()
				}
		}
}
