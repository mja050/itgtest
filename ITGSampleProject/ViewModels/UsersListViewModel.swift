//
//  UsersListViewModel.swift
//  ITGSampleProject
//
//  Created by Mo'min J.Abusaada on 18/04/2022.
//

import MagicalRecord

class UsersListViewModel {
		
		var data = Box([TUser]())

		/// getData will send the request to API and get the the data as array and check the response inside the ResponseHandler class
		/// - finishRequest : this call back will return when the request return the response regardless of the result of the api is success or fail
		func getData(finishRequest: (()->())? = nil){
				ResponseHandler<GitHubService, TUser>.checkResponse(target: .allUsers) { [weak self] arr in
						/// check if there is no data returned from api will get the data from coredata with sorting ascending by id of user
						var allUsers = arr
						if allUsers.count == 0 {
								allUsers = (TUser.mr_findAllSorted(by: "id", ascending: true) as? [TUser]) ?? []
						}
						/// the data will return as array and will set the data to Box generic class that notify when value changes
						self?.data.value = allUsers
				} finishRequest: {
						finishRequest?()
				}
		}
}
