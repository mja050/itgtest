//
//  ITGSampleProjectTests.swift
//  ITGSampleProjectTests
//
//  Created by Mo'min J.Abusaada on 17/04/2022.
//

import XCTest
@testable import ITGSampleProject

///
/// I did not do a unittest for the application because the operations are done in the application automatically from entering the database and not repeating and updating the data and this is all thanks to the third-party libraries used like MagicalRecord
/// 
class ITGSampleProjectTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
